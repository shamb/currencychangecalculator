# React work test - Unit testing

## Testing philosophy rules

I've chosen to use testing-library.

The test files are contained in the `__test__` directories, specifically;

* `src/__tests__` and
* `src/currencyChangeCalculator/__tests__`.

There is also a test data file at `src/testData.js`.

## App

See `src/__tests__`/App.test.js`

The main Application file App.js is a data loader, so we have to mock a server. As all data fetches are in App.js, we don't have to mock the server in any other component.

## CurrencyChangeCalculator

See `src/currencyChangeCalculator/CurrencyChangeCalculator.test.js`.

This component is a wrapper that injects default values (currently just the initially selected currency value on the top drop-down). We only need to test it renders, as any fault in the default value would fail render.

## CurrencyForm

See `src/currencyChangeCalculator/CurrencyForm.test.js`.

This component is the main application, so contains most of the interactivity testing (and in fact, most of the testing).

There are a number of tests;

### it correctly renders an instance of CurrencyForm at start up

Simple sanity check, tests for render by looking for the main container tag.

### it correctly renders a form with the required labels/fields at their expected defaults

Tests the default application before any interaction. Makes sure the entire form is rendered with the expected default values; 

* **Currency** is `gbp`.
* **Product price** and **Presented amount** are shown as cleared.
* the error text field is rendered but not displayed because its style class is `hidden`.
* The change list is not rendered.

### it correctly detects user error "product price higher than presented amount"

This test confirms users sees an error if they enter a **Product price** is higher than the **Presented amount**.

### it correctly detects user error "too many decimal places"

This test confirms users see an error if they enter a decimal value that has more than 2 decimal places.

### it populates a correct change list

This test confirms that;

1. If the user starts at the default and then enters a correct **Product price** and **Presented amount**, then they see the correct change list and no error messages.
2. If the user then changes the currency, they then see an updated change list in the new currency.
3. If the user then changes an amount, they see an updated change list with new values.

### it populates a correct change list when the user alters away from the default currency at start, then enters incorrect data

Now that we have done the main tests, we can look at the gotchas, which are;

1. The user changing the currency straight away from default.
2. The user raising an error from a previously good change list (the change list should disappear, and the error list should show with the errors).

As the main interaction tests are now done, we can limit testing of the children of **CurrencyForm** to _'throw data at it and make sure it shows the correct view'_ tests...

## CurrencyFormStatusText

Simple tests for a non zero and zero error list. We test to see no-errors and errors displayed respectively. We use full data taken from the live working system (I prefer this to using Snapshots, as per **Testing philosophy rule 4**).

## CurrencyChangeList

Another simple test where I just inject live data from the well-working system back into the component. Although this is a simple static test, the actual data injected is quite specific, so any change in later iterations would be very quickly revealed;

```javascript
{
  timeStamp:"Tue, 09 Jun 2020 19:44:52 GMT",
  currencyType:"eur",
  productPrice:10.23,
  presentedAmount:20,
  changeAmounts:[
    {"denomination":5,"count":1},
    {"denomination":2,"count":2},
    {"denomination":0.5,"count":1},
    {"denomination":0.2,"count":1},
    {"denomination":0.05,"count":1},
    {"denomination":0.01,"count":2}
  ],
  "changeTotal":9.77
}
```

Note that this data is the way it is because of a future enhancement (sending the change list out as a JSON file). That this future format is already in a current test mean that if someone changes the data format later, it will fail this test! So that _someone_ will have to consider updating the view/view-tests.
Again, a good example of my **Testing philosophy rule 4** - the need to rewrite would not be explicitly thrown as a test error by a Snapshot.
