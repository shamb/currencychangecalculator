# React Job test

A currency change calculator is a common test given to React developers as part of the job interview process. I have seen variations of it a number of times in job tests. I've uploaded my solution as it may make a good learning exercise for others.

[Click this link to use the completed application]( https://shamb.bitbucket.io/example/currency-change-calculator/).

## Problem spec - Change Calculator

A change calculator is required that handles multiple currencies. It takes a presented amount and a product price and the change returned is shown using the least number of notes/coins, highest denominations first.

**Example**: Given a presented amount of $100 and a product price of £48.31; the primary output should be:

You supplied $100, with a product price of $48.31.

This is your change:

1 x $50

1 x $1

1 x 50¢

1 x 10¢

1 x 5¢

4 x 1¢

Total change: $51.69

![Final design wireframe](./readme-assets/ui00.png)

There is a need as a secondary type of output for a forthcoming feature that returns a JSON representation of the change.  This needs to be factored in with this design.

## Analysing the problem

- The code should be extensible, so should be *data driven* with zero hardcodes. In particular, the code should be able to accept more currencies just by changing the config.
- This is best implemented (at least for this exercise) as something that is run manually by the cash register operator on a laptop/phone.
- It would be useful to be able to specify instances when the cashier cannot give optimal change (because there is not enough of each denomination in the till). I'll specify this in config enough to give a good idea on how to set it up, but not implement it (as I've never seen it as part of the question).

## Data schema

It always makes sense to define your data (config, state, class properties) before you start coding. Here's my first attempt before coding. I am assuming £ (Great Britain Pounds or 'gpb'), $ (US Dollars or 'usd') and € (Euros or 'eur') as the available currencies.

### Config

#### availableDenominations

```javascript
{
  gbp:  [50, 20, 10, 5, 2, 1, 0.50, 0.20, 0.10, 0.05, 0,02, 0.01],
  usd:  [100, 50, 20, 10, 5, 1, 0.5, 0.25, 0.1, 0.05, 0.01],
  eur:  [500, 200, 100, 50, 20, 10, 5, 2, 1, 0.50, 0.20, 0.10, 0.05, 0.01],
}
```

The denominations are taken from Wikipedia.

#### currencyTypes

 ```javascript
[
  {value:"gbp", label:"£"},
  {value:"usd", label:"$"},
  {value:"eur", label:"€"},
];
 ```

The value is the key to `availableDenominations`. This abstracts out the currency type to data (i.e. no ugly hardcoded switch-case!).

#### currencySymbol

```javascript
{
  gbp:
    note: "£",
    coin: "p",
  },
  {
    usd: {
    note: "$",
    coin: "¢",
  }
  eur: {
    note: "€",
    coin: "c",
  },
}
```

### Data

#### currencyType

A string representing the current currency type. Must be a `currencyTypes` value.

#### productPrice

The order value to 0.01.

#### presentedAmount

The presented cash amount to 0.01.

#### availableFloat

If we wanted to add an available float feature later, it would be of the form below (the order of the denominations is the same as that in `availableDenominations`). The cashier can enter the number of each denomination at the start of the day, and for any or all  currencies. If they don't enter them, the default value will be the MAX_SAFE_NUMBER (which means the code will assume an infinite float). There would also need to be be a feature to top up the cash register with additional float notes throughout the day.

```javascript
{
  gpb:[
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
    Number.MAX_SAFE_NUMBER,
  ],
  usd:[
    ...
  ],
  eur:[
    ...
  ]  
}
```

#### changeAmounts

The spec says the change amount needs to be extensible enough to be sent out as JSON. This means the change amount may have to be;

- Printable by another system,

- Validatable by another system,

- Colatable (i.e. a number of different cash register histories should be mergeable into a single master list)

The actual change is an array of denominations (e.g. 10x2 and 0.05x1 for $20.05 change).

```javascript
{
  timestamp: // string
  currencyType: // string enum: one of ["gbp", "eur", "usd"]
  productPrice: // number
  presentedAmount: // number
  changeAmount:
  [
    ...
    {
      denomination: number // the note or coin, e.g. 10 or 0.1
      count: number // the number of the note/coin needed in the change.
    }
  ]
}
```

## POC/Styling

Most of this can be implemented as a single JavaScript very easily, and this allows us to test styling. More importantly, doing this allows us to sort out styling and look-and-feel quickly (and get sign-off!) before committing to potentially much more expensive rework of front end and back end code!

The basic application mockup (vanilla JS and CSS) is in a single html file, `designSteps/pureJS/index.html`.

NB - the form updates on blur - there is no submit button. I don't need to fix this for the POC because _its a POC_.

*The basic form.*  

![basic ui](./readme-assets/ui01.png)

*The currency dropdown.*

![basic ui currency dropdown](./readme-assets/ui02.png)


*The fields act as number input on focus but revert to text (currency) display on blur.*
![basic ui currency value formatting](./readme-assets/ui03.png)

![basic ui currency value formatting](./readme-assets/ui04.png)

*There is an info/warning field when the user enters incorrect information*

![basic ui currency value formatting](./readme-assets/ui05.png)

*Changing the currency type causes immediate recalculation in the new currency*

![basic ui currency value formatting](./readme-assets/ui06.png)

![basic ui currency value formatting](./readme-assets/ui07.png)

## Final application

I chose to use Function components throughout.

To build the final application, I;

- Split out our currency config into reasonable sounding API calls and placed them as mock JSON in `public/api/currency`

- Used App.jsx as a data loader.

- Inside App we have CurrencyChangeCalculator. Any application hardcode flags are set here. at the moment there is only 1 (the currency that is selected on start), but this would grow as the application got larger. CurrencyChangeCalculator renders CurrencyForm. This is our main component. 

- As App does all the async stuff, CurrencyForm (and all its child components) can assume all data appears immediately, and because of CurrencyChangeCalculator, it has no hard-codes so can be dropped into another application (or *could* once I made the styling portable - see 'what I did not do' below).

- CurrencyChangeCalculator contains all state, and renders the main form. It delegates render of a validation error box to CurrencyFormStatusText, and the render of the change-list to CurrencyChangeList.

The final CSS has been developed further graphically a little, and now caters for mobile phone screens.

Note that the application logs the changeAmount JSON to the console as a potential 'future feature in progress'. You can see it commented out in `CurrencyForm.js`;

```javascript
if (!hasErrors) {
  calculateChangeList();
  if (changeList.current.changeAmounts.length > 0) {
    /** This logs the JSON for future expansion **/
    //console.log("The changeList expressed as JSON is", JSON.stringify(changeList.current));
  }
}
```

Here's how the React application on desktop;

![final desktop app, error](./readme-assets/ui09.png)

![final desktop app, errors](./readme-assets/ui10.png)

![final desktop app, no-errors](./readme-assets/ui11.png)

![final desktop app, no-errors-2](./readme-assets/ui12.png)

The layout changes for mobile resolutions as shown;

![final mobile app, error](./readme-assets/ui13.png)

![final mobile app, errors](./readme-assets/ui14.png)

![final mobile app, no-errors](./readme-assets/ui15.png)

![final mobile app, no-errors-2](./readme-assets/ui16.png)

## What I did not do / bug-list

I could do better in the following areas;

- I have not considered splitting out my styling - it is all in App.css. I should really refactor this into either css modules or use styled-components, both rewritten to have one css file per component.
- CurrencyForm looks like it may need splitting out a little in refactor (some of which is noted in the next bullet point below).
- The two input fields do currency formatting. They should be refactored to CurrencyInput components, and the bug below fixed in the process.

> Unit testing is covered in separate readme file `README-UNIT-TESTS.md`.

I have the following bug-list;

- The form items take in a number input, but immediately format them into currency on blur. As a number, they allow more than two decimal places, which correctly raises a validation error. However, the currency formatting on blur reformats to two decimal places, but the validation error remains in the view. I think I should rewrite the input fields as general CurrencyInputs, fixing the error in the process (the displayed values should be part of state, so their changing on blur from number to formatted currency should be picked up).
- Although the input number fields have a minimum value of zero, the user can by pass this by adding a '-' (minus sign) or pasting a negative number into the field.
