// libs
import React from 'react';
// Testing libs
import { render, toBeInTheDocument, prettyDOM, getByText, waitFor, cleanup, act, getByTestId, screen } from '@testing-library/react';
import { rest } from 'msw'
import { setupServer } from 'msw/node'

// Application
import App from '../App';
// Application data
import { availableDenominationsData, currencySymbolsData, currencyTypesData } from "../testData";


describe('The app renders correctly at start-up', () => {
  const server = setupServer(
    rest.get("/api/currency/availableDenominations.json", (req, res, ctx) => res(ctx.json(availableDenominationsData))),
    rest.get("/api/currency/currencySymbols.json", (req, res, ctx) => res(ctx.json(currencySymbolsData))),
    rest.get("/api/currency/currencyTypes.json", (req, res, ctx) => res(ctx.json(currencyTypesData)))
  );
  // Spy to make sure the correct resource urls are hit
  beforeEach(() => {
    jest.spyOn(window, 'fetch');
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  // Setup the mock server
  beforeAll(() => server.listen());
  /*
  beforeEach(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());
  */

  // Create and cleanup the application under test.
  beforeEach(() => {
    render(<App />);
  });
  afterEach(() => {
    act(() => {
      cleanup();
    })
  });

  test('confirms app exists', () => {
    expect(screen.getByTestId("app")).toBeInTheDocument();
  });

  test('confirms initial loading page exists', async () => { 
    await waitFor(() => {
      expect(screen.getByText("Loading data..."));
    });
    
  });

  test('confirms the application requests the expected data', async () => {
    await waitFor(() => {
      expect(window.fetch).toHaveBeenCalledTimes(3);
      expect(window.fetch.mock.calls[0][0]).toEqual('/api/currency/availableDenominations.json');
      expect(window.fetch.mock.calls[1][0]).toEqual('/api/currency/currencySymbols.json');
      expect(window.fetch.mock.calls[2][0]).toEqual('/api/currency/currencyTypes.json');
    });
  });

  test('confirms the application moves to the main page on data load', async () => {
    await waitFor(() => {
      const startUpText1 = screen.queryAllByText("Loading data...");
      const startUpText2 = screen.queryAllByText("Data load failed. Please refresh after a few seconds, or contact us.");
      // Look for first text expected to appear in fully started up screen
      expect(screen.getByText("Currency"));
      // Confirm the two start-up messages are not seen.
      expect(startUpText1.length).toBe(0);
      expect(startUpText2.length).toBe(0);
    })
  })
  
});


describe("it displays correctly on a data fault", () => {
  const server = setupServer(
    rest.get("/api/currency/availableDenominations.json", (req, res, ctx) => res(ctx.status(500))),
    rest.get("/api/currency/currencySymbols.json", (req, res, ctx) => res(ctx.status(500))),
    rest.get("/api/currency/currencyTypes.json", (req, res, ctx) => res(ctx.status(500)))
  );
  // Setup the mock server
  beforeEach(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());
  // Create the application under test.
  beforeEach(() => {
    render(<App />);
  });
  // Cleanup the application under test.
  afterEach(() => {
    cleanup();
  })

  test("displays the correct screen on load fail", async () => {
    await waitFor(() => {
      const startUpText1 = screen.queryAllByText("Loading data...");
      const startUpText2 = screen.queryAllByText("Data load failed. Please refresh after a few seconds, or contact us.");
      const startUpText3 = screen.queryAllByText("Currency");

      expect(startUpText1.length).toBe(1);
      expect(startUpText2.length).toBe(1);
      expect(startUpText3.length).toBe(0);
    });
  });

});
