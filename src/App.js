// libs
import React, { useState, useRef, useEffect } from "react";
// application
import CurrencyChangeCalculator from "./currencyChangeCalculator/CurrencyChangeCalculator";
// styling
import "./App.css";

// statics
const configURLs = [
  "/api/currency/availableDenominations.json",
  "/api/currency/currencySymbols.json",
  "/api/currency/currencyTypes.json",
];

/**App acts as loader for config. Once the three config files are loaded,
 * the CurrencyChangeCalculator hits the page.
 */

function App() {
  const [dataLoaded, setDataLoaded] = useState(false);
  const [dataLoadFailed, setDataLoadFailed] = useState(false);
  const availableDenominations = useRef();
  const currencyTypes = useRef();
  const currencySymbols = useRef();

  useEffect(() => {
    // NB - isMounted is to avoid the 'Can't perform a React state
    // update on an unmounted component' warning during unit testing.
    const aborter = new AbortController();
    let isMounted = true;

    fetchURLs(configURLs, aborter).then(
      (response) => {
        availableDenominations.current = response[0];
        currencySymbols.current = response[1];
        currencyTypes.current = response[2];
        if (isMounted) {
          setDataLoaded(true);
        }
      },
      () => {
        if (isMounted) {
          setDataLoadFailed(true);
        }
      }
    );
    return () => {
      // cleanup on unmount (mainly for unit tests).
      isMounted = false;
      aborter.abort();
    };
  }, []);

  return (
    <div className="appContainer">
      <div className="app" data-testid="app">
        {!dataLoaded && <p>Loading data...</p>}
        {dataLoadFailed && (
          <p>
            Data load failed. Please refresh after a few seconds, or contact us.
          </p>
        )}
        {dataLoaded && (
          <CurrencyChangeCalculator
            availableDenominations={availableDenominations.current}
            currencyTypes={currencyTypes.current}
            currencySymbols={currencySymbols.current}
          />
        )}
      </div>
    </div>
  );
}

// helper functions

async function fetchURLs(urls, abortController) {
  const fetches = urls.map((url) =>
    fetch(url, { signal: abortController.signal }).then((response) =>
      response.json()
    )
  );
  const data = await Promise.all(fetches);
  return data;
}

export default App;
