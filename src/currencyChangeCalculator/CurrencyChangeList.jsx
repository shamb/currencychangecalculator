import React from 'react';
import PropTypes from 'prop-types';

function CurrencyChangeList({list, currencySymbols}) {
  const listItems = makeChangeListItems(list, currencySymbols);
  const symbols = currencySymbols[list.currencyType];
  const presentedAmountDisplay = formattedCurrency(list.presentedAmount, symbols);
  const productPriceDisplay = formattedCurrency(list.productPrice, symbols);
  const changeDisplay = formattedCurrency(list.changeTotal, symbols);
  const supplyString = `You supplied ${presentedAmountDisplay} with a product price of ${productPriceDisplay}.`;

  return (
    <div data-testid="currencyChangeList" className="change-list-container">
      <p className="change-list-box">{supplyString}</p>
      <p className="change-list-header change-list-box">This is your change;</p>
      <ul className="change-list change-list-box">
        {listItems}
      </ul>
      <p className="change-total change-list-box">Total: {changeDisplay}</p>
    </div>
  );
}

CurrencyChangeList.propTypes = {
  list:PropTypes.object.isRequired,
  currencySymbols:PropTypes.object.isRequired
};


// Helpers


function makeChangeListItems(list, currencyTypes) {
  const changeAmounts = list.changeAmounts;
  const symbols = currencyTypes[list.currencyType];
  let listItems;

  listItems = changeAmounts.map(item => {
    const itemValue = (item.denomination >= 1) ? `${symbols.note}${item.denomination}` : `${item.denomination*100}${symbols.coin}`;
    const label = `${item.count} x ${itemValue}`;
    const key=`key_${item.denomination}`;

    return <li key={key} className="change-list item">{label}</li>
  });
  return listItems;
}

function formattedCurrency(value, symbols) {
  let  displayText;

  if (value < 1) {
    displayText = `${(value*100).toFixed(0)}${symbols.coin}`
  } else {
    displayText = `${symbols.note}${value.toFixed(2)}`;
  }
  return displayText;
}

export default CurrencyChangeList;