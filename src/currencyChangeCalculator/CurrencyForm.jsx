// libs
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
// application
import CurrencyFormStatusText from './CurrencyFormStatusText';
import CurrencyChangeList from './CurrencyChangeList';

const ERROR_DEBOUNCE = 500;

function CurrencyForm({availableDenominations, currencySymbols, currencyTypes, initialCurrency}) {
  const [currencyOptions, setCurrencyOptions] = useState([]);
  const [currencyType, setCurrencyType] = useState(initialCurrency);
  const [productPrice, setProductPrice] = useState(0);
  const [presentedAmount, setPresentedAmount] = useState(0);
  const [errors, setErrors] = useState([]);
  const changeList = useRef([]);
  const validationDelay = useRef(null);
  const hasErrors = useRef(errors.length > 0);
  const shouldShowChange = useRef(!hasErrors && productPrice!==0 && presentedAmount!==0);

  
  useEffect(()=> {
    function makeCurrencyInput(field) {
      /** turn field into a number input on focus **/
      field.addEventListener("focus", ()=> {
        field.setAttribute("type", "number");
        field.setAttribute("step", "0.01");
        field.setAttribute("min", "0");
      });

      /** Convert the input into a currency string on blur **/
      field.addEventListener("blur", ()=> {
        field.removeAttribute("step");
        field.removeAttribute("min");
        field.setAttribute("type", "text");
        field.value = Number(field.value).toFixed(2);
      });
    }

    /** Initialise DOM **/
    const productPriceUI = document.getElementById("productPrice");
    const presentedAmountUI = document.getElementById("presentedAmount");
    makeCurrencyInput(productPriceUI);
    makeCurrencyInput(presentedAmountUI);

    /** Cleanup on unmount **/
    return () => {
      if (validationDelay.current !==  null) {
        window.clearTimeout(validationDelay.current);
      }
    }
  }, []);


  useEffect(()=> {
    /** Initialise currency dropdown options **/
    setCurrencyOptions(currencyTypes.map(({value, label}) => <option key={label} data-testid={value} value={value}>{label}</option>));
  }, [currencyTypes])


  function calculateChangeList() {
    changeList.current = calculateChange(currencyType, productPrice, presentedAmount, availableDenominations);
  }


  /** Event handlers **/


  function onChange(e) {
    const targetID = e.target.id;
    const newValue = e.target.value;
    const newValueNum = Number(newValue || 0);
    let errors = [];
    let productPriceEntered;
    let presentedAmountEntered;   

    // console.log("change in ", targetID, "to", newValue);
    switch(targetID) {
      case "currencyType" :
        productPriceEntered = productPrice !== 0;
        presentedAmountEntered = presentedAmount !== 0;
        // set the state
        setCurrencyType(newValue);
        break;
      
      case "productPrice" :
        productPriceEntered = newValueNum !==0;
        presentedAmountEntered = presentedAmount !==0
        // set the state
        setProductPrice(newValueNum);
        // validation
        errors = errors.concat(checkForValidCurrency("Product price", newValue));
        if (presentedAmount !== 0 && newValueNum > presentedAmount) {
          errors.push("Presented amount must be higher than product price.");
        }
        break;

      case "presentedAmount" :
        productPriceEntered = productPrice !== 0;
        presentedAmountEntered = newValueNum !== 0;
        // set the state
        setPresentedAmount(newValueNum);
        // validation
        errors = errors.concat(checkForValidCurrency("Presented amount", newValue));
        if (newValueNum !== 0 && (productPrice > newValueNum)) {
          errors.push("Presented amount must be higher than product price.");
        }
        break;

      default :
        // do nothing (we should probably throw an error if we are running on localhost).
    }

    hasErrors.current = errors.length > 0;
    shouldShowChange.current =  !hasErrors.current && productPriceEntered && presentedAmountEntered;
    
    // Debounce errors (so the user does not see errors on partially typed values)
    if (validationDelay.current !== null) {
      window.clearTimeout(validationDelay.current);
    }
    validationDelay.current = window.setTimeout(() => {
      setErrors(errors);
      validationDelay.current = null;
    },ERROR_DEBOUNCE);
  }


  /** Render code **/

  if (!hasErrors.current) {
    calculateChangeList();
    if (changeList.current.changeAmounts.length > 0) {
      /** This logs the JSON for future expansion **/
      //console.log("The changeList expressed as JSON is", JSON.stringify(changeList.current));
    }
  } 

  return (
    <>
      <form className="calculator-form" data-testid="calculator-form">

        <div className="calculator-row">
          <label htmlFor="currencyType">Currency</label>
          <select id="currencyType" data-testid="currencyType" onChange={onChange} value={currencyType}>{currencyOptions}</select>
        </div>
        
        <div className="calculator-row">
          <label htmlFor="productPrice">Product price</label>
          <input id="productPrice" data-testid="productPrice" type="text" autoComplete="off" onChange={onChange}/>
        </div>

        <div className="calculator-row">
          <label htmlFor="presentedAmount">Presented amount</label>
          <input id="presentedAmount" data-testid="presentedAmount" type="text" autoComplete="off" onChange={onChange}/>
        </div>

      </form>
      <CurrencyFormStatusText errors={errors} show={errors.length>0 && !shouldShowChange.current} />
      {shouldShowChange.current && !hasErrors.current && <CurrencyChangeList list={changeList.current} currencySymbols={currencySymbols}/>}
    </>
  );

}

CurrencyForm.propTypes = {
  availableDenominations: PropTypes.object.isRequired,
  currencySymbols: PropTypes.object.isRequired,
  currencyTypes: PropTypes.array.isRequired,
  initialCurrency: PropTypes.string.isRequired,
};


// Helpers


function checkForValidCurrency(fieldName, valueStr) {
  let errors = [];
  if (valueStr.includes(".") && valueStr.slice(valueStr.indexOf(".")+1).length > 2) {
    errors.push(`${fieldName} - too many decimal places.`);
  }
  return errors;
}


function fixCurrencyRounding(val) {
  /** fixed IEEE-754 rounding errors **/
  return Number(val.toFixed(2));
}

function calculateChange(currencyType, productPrice, presentedAmount, availableDenominations) {
  const denominations = availableDenominations[currencyType];
  const denominationsLength = denominations.length;
  const changeTotal =  fixCurrencyRounding(presentedAmount - productPrice);
  let changeAmounts = [];
  let timeStamp = new Date().toUTCString();
  let count = 0;
  let denominationIndex = 0;
  let runningChangeTotal = changeTotal;
  
  while (runningChangeTotal > 0 && denominationIndex < denominationsLength) {
    const denomination = denominations[denominationIndex];
    if (denomination <= runningChangeTotal) {
      runningChangeTotal = fixCurrencyRounding(runningChangeTotal-denomination);
      count ++;
    } 
    if (denomination > runningChangeTotal) {
      if (count > 0) {
        changeAmounts.push({denomination, count});
        count = 0;
      }
      denominationIndex ++;
    }
  }

  return {
    timeStamp,
    currencyType, 
    productPrice,
    presentedAmount,
    changeAmounts,
    changeTotal,
  };
}


export default CurrencyForm;
export { ERROR_DEBOUNCE }; // exported for unit tests.