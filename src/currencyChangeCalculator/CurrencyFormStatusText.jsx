import React, {Fragment} from 'react';
import PropTypes from 'prop-types';


function CurrencyFormStatusText({errors, show}) {
  const statusClassName = show ? "status-message" : "hidden";
  const statusText = errors.map(item => <Fragment key={item}>{item}<br/></Fragment>);

  return (
    <p className={statusClassName} data-testid="currencyFormStatusText">{statusText}</p>
  );

}

CurrencyFormStatusText.propTypes = {
  errors: PropTypes.array.isRequired,
  show: PropTypes.bool.isRequired,
};

export default CurrencyFormStatusText;
