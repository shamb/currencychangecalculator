// libs
import React from "react";
// Testing libs
import {
  render,
  waitFor,
  fireEvent,
  prettyDOM,
  getByTestId,
  getByText,
  queryByTestId,
  screen,
  cleanup,
  act,
} from "@testing-library/react";

// Application
import CurrencyForm, { ERROR_DEBOUNCE } from "../CurrencyForm";
// test data
import {
  availableDenominationsData,
  currencySymbolsData,
  currencyTypesData,
} from "../../testData";

const INITIAL_CURRENCY = "gbp";
const DEBOUNCE_DELAY = ERROR_DEBOUNCE + 100;

describe("The CurrencyForm", () => {
  beforeEach(() => {
    render(
      <CurrencyForm
        availableDenominations={availableDenominationsData}
        currencySymbols={currencySymbolsData}
        currencyTypes={currencyTypesData}
        initialCurrency={INITIAL_CURRENCY}
      />
    );
  });

  afterEach(() => {
    cleanup();
  });

  beforeAll(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  test("it correctly renders an instance of CurrencyForm at start up", () => {
    expect(screen.getByTestId("calculator-form"));
  });

  test("it correctly renders a form with the required labels/fields at their expected defaults", () => {
    // currency dropdown
    expect(screen.getByText("Currency"));
    expect(screen.getByTestId("currencyType"));
    currencyTypesData.forEach((item) => expect(screen.getByTestId(item.value)));
    expect(screen.getByTestId("currencyType").value).toBe(INITIAL_CURRENCY);
    // Product price
    expect(screen.getByText("Product price"));
    expect(screen.getByTestId("productPrice"));
    expect(screen.getByTestId("productPrice").value).toBe("");
    // Presented amount
    expect(screen.getByText("Presented amount"));
    expect(screen.getByTestId("presentedAmount"));
    expect(screen.getByTestId("presentedAmount").value).toBe("");
    // status text (should be there but not visible via a class name of 'hidden').
    expect(screen.getByTestId("currencyFormStatusText").className).toBe(
      "hidden"
    );
    // change list (should not appear)
    // NB - we have to use queryByTestId because getByTestId throws on null.
    expect(screen.queryByTestId("currencyChangeList")).toBe(null);
  });

  test("it correctly changes the input fields between type text/number on deselection/selection", async () => {
    const productPrice = screen.getByTestId("productPrice");
    const presentedAmount = screen.getByTestId("presentedAmount");
    expect(productPrice.type).toBe("text");
    expect(productPrice.hasAttribute("step")).toBe(false);
    expect(presentedAmount.type).toBe("text");
    expect(presentedAmount.hasAttribute("step")).toBe(false);
    // Check inputs on focus/change
    fireEvent.focus(productPrice);
    fireEvent.focus(presentedAmount);
    fireEvent.change(productPrice, { target: { value: "10.1" } });
    fireEvent.change(presentedAmount, { target: { value: "10.10" } });
    expect(productPrice.type).toBe("number");
    expect(productPrice.step).toBe("0.01");
    expect(productPrice.value).toBe("10.1");
    expect(presentedAmount.type).toBe("number");
    expect(presentedAmount.step).toBe("0.01");
    expect(presentedAmount.value).toBe("10.10");
    // Check inputs on blur
    fireEvent.blur(productPrice);
    fireEvent.blur(presentedAmount);
    expect(productPrice.type).toBe("text");
    expect(productPrice.hasAttribute("step")).toBe(false);
    expect(productPrice.value).toBe("10.10");
    expect(presentedAmount.type).toBe("text");
    expect(presentedAmount.hasAttribute("step")).toBe(false);
    expect(presentedAmount.value).toBe("10.10");
  });

  test("it correctly detects user error product price lower than presented amount", async () => {
    const productPrice = screen.getByTestId("productPrice");
    const presentedAmount = screen.getByTestId("presentedAmount");
    let statusText;
    // simulate user interaction.
    fireEvent.change(productPrice, { target: { value: "10" } });
    fireEvent.change(presentedAmount, { target: { value: "5" } });
    // check UI for the expected error message, plus no-show on the change list
    await waitFor(() => screen.getByTestId("currencyFormStatusText"));
    statusText = screen.getByTestId("currencyFormStatusText");
    act(() => jest.advanceTimersByTime(DEBOUNCE_DELAY));
    expect(statusText.className).toBe("status-message");
    expect(
      screen.getByText("Presented amount must be higher than product price.")
    );
    expect(screen.queryByTestId("currencyChangeList")).toBe(null);
    fireEvent.change(productPrice, { target: { value: "4" } });
    expect(statusText.className).toBe("hidden");
    fireEvent.change(productPrice, { target: { value: "9" } });
    act(() => jest.advanceTimersByTime(DEBOUNCE_DELAY));
    expect(
      screen.getByText("Presented amount must be higher than product price.")
    );
  });

  test("It correctly fills in fields if the user interacts with inputs without entering a value", () => {
    const productPrice = screen.getByTestId("productPrice");
    const presentedAmount = screen.getByTestId("presentedAmount");
    // simulate user interaction.
    fireEvent.change(productPrice, { target: { value: "" } });
    fireEvent.change(presentedAmount, { target: { value: "" } });
    fireEvent.blur(productPrice);
    fireEvent.blur(presentedAmount);
    expect(productPrice.value).toBe("0.00");
    expect(presentedAmount.value).toBe("0.00");
  });

  test('it correctly detects user error "too many decimal places"', async () => {
    const productPrice = screen.getByTestId("productPrice");
    const presentedAmount = screen.getByTestId("presentedAmount");
    let statusText;
    // simulate user interaction.
    fireEvent.change(productPrice, { target: { value: "10" } });
    fireEvent.change(presentedAmount, { target: { value: "20.000" } });
    // check UI for the expected error message, plus no-show on the change list
    await waitFor(() => screen.getByTestId("currencyFormStatusText"));
    statusText = screen.getByTestId("currencyFormStatusText");
    act(() => jest.advanceTimersByTime(DEBOUNCE_DELAY));
    expect(statusText.className).toBe("status-message");
    expect(screen.getByText("Presented amount - too many decimal places."));
    expect(screen.queryByTestId("currencyChangeList")).toBe(null);
  });

  test("it populates a correct change list", async () => {
    const currencyType = screen.getByTestId("currencyType");
    const productPrice = screen.getByTestId("productPrice");
    const presentedAmount = screen.getByTestId("presentedAmount");
    // simulate the user entering correct product price and presented amount.
    fireEvent.change(productPrice, { target: { value: "10.43" } });
    fireEvent.change(presentedAmount, { target: { value: "20.00" } });
    // check for correct change, and no error text.
    await waitFor(() =>
      screen.getByText("£20.00 with a product price of £10.43", {
        exact: false,
      })
    );
    expect(screen.getByText("This is your change;"));
    expect(screen.getByText("1 x £5"));
    expect(screen.getByText("2 x £2"));
    expect(screen.getByText("1 x 50p"));
    expect(screen.getByText("1 x 2p"));
    expect(screen.getByText("Total: £9.57"));
    expect(screen.getByTestId("currencyFormStatusText").className).toBe("hidden");
    // Simulate the user changing over to $
    fireEvent.change(currencyType, { target: { value: "usd" } });
    // check for correct change, and no error text.
    await waitFor(() =>
      screen.getByText("$20.00 with a product price of $10.43", {
        exact: false,
      })
    );
    expect(screen.getByText("This is your change;"));
    expect(screen.getByText("1 x $5"));
    expect(screen.getByText("4 x $1"));
    expect(screen.getByText("1 x 50¢"));
    expect(screen.getByText("1 x 5¢"));
    expect(screen.getByText("2 x 1¢"));
    expect(screen.getByText("Total: $9.57"));
    expect(screen.getByTestId("currencyFormStatusText").className).toBe(
      "hidden"
    );
    // Check if the user correctly changes an amount, the change list updates to reflect it, with no errors shown.
    fireEvent.change(productPrice, { target: { value: "10.44" } });
    await waitFor(() =>
      screen.getByText("$20.00 with a product price of $10.44", {
        exact: false,
      })
    );
    expect(screen.getByText("This is your change;"));
    expect(screen.getByText("1 x $5"));
    expect(screen.getByText("4 x $1"));
    expect(screen.getByText("1 x 50¢"));
    expect(screen.getByText("1 x 5¢"));
    expect(screen.getByText("1 x 1¢"));
    expect(screen.getByText("Total: $9.56"));
    expect(screen.getByTestId("currencyFormStatusText").className).toBe(
      "hidden"
    );
  });

  test("it populates a correct change list when the user alters away from the default currency at start, then enters incorrect data", async () => {
    const currencyType = screen.getByTestId("currencyType");
    const productPrice = screen.getByTestId("productPrice");
    const presentedAmount = screen.getByTestId("presentedAmount");
    let statusText;
    // simulate the user changing currency, then entering correct product price and presented amount.
    fireEvent.change(currencyType, { target: { value: "eur" } });
    fireEvent.change(productPrice, { target: { value: "10.43" } });
    fireEvent.change(presentedAmount, { target: { value: "20.00" } });
    // check for correct change, and no error text.
    await waitFor(() =>
      screen.getByText("€20.00 with a product price of €10.43", {
        exact: false,
      })
    );
    expect(screen.getByText("This is your change;"));
    expect(screen.getByText("1 x €5"));
    expect(screen.getByText("2 x €2"));
    expect(screen.getByText("1 x 50c"));
    expect(screen.getByText("1 x 5c"));
    expect(screen.getByText("2 x 1c"));
    expect(screen.getByText("Total: €9.57"));
    expect(screen.getByTestId("currencyFormStatusText").className).toBe(
      "hidden"
    );
    // Sanity check - simulate a user error from a correct change list state.
    fireEvent.change(productPrice, { target: { value: "10.430" } });
    await waitFor(() => screen.getByTestId("currencyFormStatusText"));
    statusText = screen.getByTestId("currencyFormStatusText");
    act(() => jest.advanceTimersByTime(DEBOUNCE_DELAY));
    expect(statusText.className).toBe("status-message");
    expect(screen.getByText("Product price - too many decimal places."));
    expect(screen.queryByTestId("currencyChangeList")).toBe(null);
  });
});
