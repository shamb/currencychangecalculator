// libs
import React from "react";
// Testing libs
import { render, prettyDOM, getByTestId, getByText, screen, cleanup } from "@testing-library/react";

// Application
import CurrencyChangeList from "../CurrencyChangeList";
// test data
import { currencySymbolsData, changeListData, changeListDataLow } from "../../testData";

describe("CurrencyChangeList with change greater than 1", () => {

  beforeEach(() => {
    // Render the component
    render(
      <CurrencyChangeList
        currencySymbols={currencySymbolsData}
        list={changeListData}
      />
    );
  });

  afterEach(() => {
    // Cleanup the component.
    cleanup();
  });

  test("it confirms a correct change list", () => {
    expect(screen.getByTestId("currencyChangeList"));
    expect(screen.getByText("You supplied €20.00 with a product price of €10.23", {exact: false}));
    expect(screen.getByText("This is your change", {exact: false}));
    expect(screen.getByText("1 x €5"))
    expect(screen.getByText("2 x €2"))
    expect(screen.getByText("1 x 50c"))
    expect(screen.getByText("1 x 20c"))
    expect(screen.getByText("1 x 5c"))
    expect(screen.getByText("2 x 1c"))
  });
  
});


describe("CurrencyChangeList with change less than 1", () => {
  beforeEach(() => {
    render(
      <CurrencyChangeList
        currencySymbols={currencySymbolsData}
        list={changeListDataLow}
      />
    );
  });
  // Cleanup the application under test.
  afterEach(() => {
    cleanup();
  });

  test("it confirms a correct change list", () => {
    expect(screen.getByTestId("currencyChangeList"));
    expect(screen.getByText("You supplied €1.00 with a product price of 55c", {exact: false}));
    expect(screen.getByText("This is your change", {exact: false}));
    expect(screen.getByText("2 x 20c"))
    expect(screen.getByText("1 x 5c"))
  });
  
});

