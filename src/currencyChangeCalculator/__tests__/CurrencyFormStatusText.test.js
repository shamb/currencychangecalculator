// libs
import React from "react";
// Testing libs
import {
  render,
  getByTestId,
  getByText,
  cleanup,
  screen,
} from "@testing-library/react";

// Application
import CurrencyFormStatusText from "../CurrencyFormStatusText";

const ERROR_LIST_POPULATED = [
  "Error 1 - Some error text 1",
  "Error 2 - Some error text 2",
];

const ERROR_LIST_EMPTY = [];

describe("CurrencyFormStatusText for a populated-displayed error list", () => {
  beforeEach(() => {
    render(
      <CurrencyFormStatusText errors={ERROR_LIST_POPULATED} show={true} />
    );
  });
  afterEach(() => {
    cleanup();
  });

  test("it confirms error list is displayed for a populated-displayed error list", () => {
    const currencyFormStatusText = screen.getByTestId("currencyFormStatusText");
    expect(currencyFormStatusText.className).toBe("status-message");
    expect(screen.getByText("Error 1", { exact: false }));
    expect(screen.getByText("Error 2", { exact: false }));
  });
});

describe("CurrencyFormStatusText for an unpopulated-shown list", () => {
  beforeEach(() => {
    render(<CurrencyFormStatusText errors={ERROR_LIST_EMPTY} show={true} />);
  });

  afterEach(() => {
    cleanup();
  });

  test("it confirms no text is displayed for an unpopulated-shown list", () => {
    const currencyFormStatusText = screen.getByTestId("currencyFormStatusText");
    expect(currencyFormStatusText.className).toBe("status-message");
    expect(currencyFormStatusText.innerText).toBe(undefined);
  });
});

describe("CurrencyFormStatusText for a populated-not-shown list", () => {
  beforeEach(() => {
    render(<CurrencyFormStatusText errors={ERROR_LIST_POPULATED} show={false} />);
  });

  afterEach(() => {
    cleanup();
  });

  test("it confirms no text is displayed for a populated-not-shown list", () => {
    const currencyFormStatusText = screen.getByTestId("currencyFormStatusText");
    expect(currencyFormStatusText.className).toBe("hidden");
  });
});
