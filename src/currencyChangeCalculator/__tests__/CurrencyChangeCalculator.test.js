// libs
import React from 'react';
// Testing libs
import { render, prettyDOM, getByTestId, screen } from '@testing-library/react';

// Application
import CurrencyChangeCalculator from '../CurrencyChangeCalculator';
// test data
import { availableDenominationsData, currencySymbolsData, currencyTypesData } from  '../../testData';

describe('The CurrencyChangeCalculator renders at start-up', () => {

  beforeEach(() => {
    render(
      <CurrencyChangeCalculator 
        availableDenominations = {availableDenominationsData}
        currencyTypes={currencyTypesData}
        currencySymbols={currencySymbolsData}
      />
    );
  });



  test('it correctly renders an instance of CurrencyChangeCalculator', () => {
    expect(screen.getByTestId("calculator-form"))
  });

});