// libs
import React from 'react';
import PropTypes from 'prop-types';
// application
import CurrencyForm from "./CurrencyForm";
//styling

const INITIAL_CURRENCY_INDEX = 0;


function CurrencyChangeCalculator({availableDenominations, currencySymbols, currencyTypes}) {
  
  return (
    <>
      <CurrencyForm
        availableDenominations={availableDenominations}
        currencySymbols={currencySymbols}
        currencyTypes={currencyTypes}
        initialCurrency={currencyTypes[INITIAL_CURRENCY_INDEX].value}
        />
    </>
  );
}

CurrencyChangeCalculator.propTypes = {
  availableDenominations: PropTypes.object.isRequired,
  currencySymbols: PropTypes.object.isRequired,
  currencyTypes: PropTypes.array.isRequired,
};

export default CurrencyChangeCalculator;