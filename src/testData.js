const availableDenominationsData = {
  "gbp": [
    50,
    20,
    10,
    5,
    2,
    1,
    0.5,
    0.2,
    0.1,
    0.05,
    0.02,
    0.01
  ],
  "usd": [
    100,
    50,
    20,
    10,
    5,
    1,
    0.5,
    0.25,
    0.1,
    0.05,
    0.01
  ],
  "eur": [
    500,
    200,
    100,
    50,
    20,
    10,
    5,
    2,
    1,
    0.5,
    0.2,
    0.1,
    0.05,
    0.01
  ]
};

const currencySymbolsData = {
  "gbp": {
    note: "£",
    coin: "p"
  },
  "usd": {
    note: "$",
    coin: "¢"
  },
  "eur": {
    note: "€",
    coin: "c"
  }
};

const currencyTypesData = [
  {
    value: "gbp",
    label: "£"
  },
  {
    value: "usd",
    label: "$"
  },
  {
    value: "eur",
    label: "€"
  }
];

const changeListData = {
  timeStamp:"Tue, 09 Jun 2020 19:44:52 GMT",
  currencyType:"eur",
  productPrice:10.23,
  presentedAmount:20,
  changeAmounts:[
    {"denomination":5,"count":1},
    {"denomination":2,"count":2},
    {"denomination":0.5,"count":1},
    {"denomination":0.2,"count":1},
    {"denomination":0.05,"count":1},
    {"denomination":0.01,"count":2}
  ],
  "changeTotal":9.77
}

const changeListDataLow ={
  timeStamp:"Thu, 03 Dec 2020 21:28:25 GMT",
  currencyType:"eur",
  productPrice:0.55,
  presentedAmount:1,
  changeAmounts:[
    {"denomination":0.2,"count":2},
    {"denomination":0.05,"count":1}
  ],
  "changeTotal":0.45
};

export { availableDenominationsData, currencySymbolsData, currencyTypesData, changeListData, changeListDataLow };